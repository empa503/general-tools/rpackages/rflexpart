#
#' @export
read.totals = function(fn){

    dat = NULL
    for (jj in 1:length(fn)){
        hdr = read.header(file.path(dirname(fn[jj]), "header"))

        strm = file(fn[jj], open="rb")
        seek(strm, origin="current", where=4)
        tmp = matrix(NA, ncol=1+10*hdr$nspec, nrow=100000)
        ii = 1
        repeat{
            itime = readBin(strm, "integer", n=1, size=4)
            tmpstr = readBin(strm, "numeric", n=10*hdr$nspec, size=8)    
        
            if (length(tmpstr)==0) break
        
            tmp[ii, ] = c(itime,tmpstr)

            seek(strm, origin="current", where=8)
            ii = ii + 1
        }
        close(strm)
        tmp = tmp[1:(ii-1),]
        colnames(tmp) = c("itime", paste("mass_", hdr$species.name, sep=""), 
                            paste("OH_loss_", hdr$species.name, sep=""),
                            paste("OH_gain_", hdr$species.name, sep=""),
                            paste("Cl_loss_", hdr$species.name, sep=""),
                            paste("Cl_gain_", hdr$species.name, sep=""),
                            paste("hydro_loss_", hdr$species.name, sep=""),
                            paste("hydro_gain_", hdr$species.name, sep=""),
                            paste("wetdep_loss_", hdr$species.name, sep=""),
                            paste("drydep_loss_", hdr$species.name, sep=""),
                            paste("eol_loss_", hdr$species.name, sep="")
                  )
        tmp = as.data.frame(tmp)
        tmp$dtm = as.chron(hdr$dtm + tmp$itime/86400)
        
        msk = which(regexpr("(em_up)|(em_field)|(loss_)|(gain_)", names(tmp))>0)
        tmp[,msk] = tmp[,msk]/hdr$loutsample

        if (is.null(dat)) {
            dat = tmp
        } else {
            dat = rbind(dat, tmp)
        }
    }
    
    return(dat)
}
