write.particle.dump = function(part, fn, is.cosmo=FALSE, is.inst=FALSE, is.ctm=FALSE){
	FORT.stop = as.raw(c(12,0,0,0))

	npart = nrow(part)
	msk.mass = which(grepl("mass_", colnames(part)))
	msk.para = which(colnames(part) %in% c("oro", "pv", "qq", "rho", "hmix", "tropo", "tt"))
	nspec = length(msk.mass)
	if (!is.null(attr(part, "itime"))){
		itime = as.integer(attr(part, "itime"))
	} else {
		itime = as.integer(0)
	}

	if (is.ctm){
		
	}

    strm = file(fn, open="wb")

	if (is.ctm){
		block.length = as.integer(12)
		writeBin(block.length, strm, size=4)
		writeBin(as.integer(c(itime, nspec, 0)), strm, size=4)
	} else {
		block.length = as.integer(4)
		writeBin(block.length, strm, size=4)
		writeBin(itime, strm, size=4)
	}
	writeBin(block.length, strm, size=4)

	part = as.matrix(part)	

	sapply(1:npart, function(i, strm, part) {
		block.length = as.integer(4*(5+length(msk.para)+length(msk.mass)))
		writeBin(block.length, strm, size=4)
		writeBin(as.integer(part[i,"npoint"]), strm, size=4)
	    writeBin(as.vector(part[i,c("x", "y", "z")]), strm, size=4)	
		writeBin(as.integer(part[i,"itramem"]), strm, size=4)
	    writeBin(as.vector(part[i, c(msk.para,msk.mass)]), strm, size=4)	
		writeBin(block.length, strm, size=4)
		}, strm=strm, part=part)

	#	write stop line
	block.length = as.integer(4*(5+length(msk.para)+length(msk.mass)))
	writeBin(block.length, strm, size=4)
	writeBin(as.integer(-99999), strm, size=4)
    writeBin(rep(-9999.9,3), strm, size=4)	
	writeBin(as.integer(-99999), strm, size=4)
    writeBin(rep(-9999.9, length(c(msk.para,msk.mass))), strm, size=4)	
	writeBin(block.length, strm, size=4)

	close(strm)

}


#require(Rflexpart)
#source("~/svn_empa/Rpackages/Rflexpart/R/read.particle.dump.R")
#source("~/svn_empa/Rpackages/Rflexpart/R/write.FLEXPART_header.R")
#source("~/svn_empa/Rpackages/Rflexpart/R/read.header.R")
#options(error=recover)
#
#
#path.in = "/mnt/project/hes/MAIOLICA2/output/Nudging/original"
#path.out = "/mnt/project/hes/MAIOLICA2/output/Nudging/20130101"
#
#hdr = read.header(file.path(path.in, "header"), remove.space=FALSE)
#part = read.particle.dump(file.path(path.in, "partposit_end.gz"), hdr=hdr)
#
#
##	combine tracers
#tmp = combine.tracers(hdr=hdr, part=part, 
#	ct=list(CH4_A1_1=paste("CH4_A1", 1:5, sep="_"), 
#		CH4_A2_1=paste("CH4_A2", 1:5, sep="_"),
#		CH4_A3_1=paste("CH4_A3", 1:5, sep="_"),
#		CH4_A4_1=paste("CH4_A4", 1:5, sep="_"),
#		CH4_A5_1=paste("CH4_A5", 1:5, sep="_"),
#		CH4_A6_1=paste("CH4_A6", 1:5, sep="_"),
#		CH4_A7_1=paste("CH4_A7", 1:5, sep="_"),
#		CH4_A8_1=paste("CH4_A8", 1:5, sep="_"),
#		CH4_A9_1=paste("CH4_A9", 1:5, sep="_"), 
#		CH4_A10_1=paste("CH4_A10", 1:5, sep="_"), 
#		CH4_A11_1=paste("CH4_A11", 1:5, sep="_"), 
#		CH4_B1_1=paste("CH4_B1", 1:5, sep="_"), 
#		CH4_B2_1=paste("CH4_B2", 1:5, sep="_"),
#		CH4_B3_1=paste("CH4_B3", 1:5, sep="_"),
#		CH4_B4_1=paste("CH4_B4", 1:5, sep="_"),
#		CH4_B5_1=paste("CH4_B5", 1:5, sep="_"),
#		CH4_B6_1=paste("CH4_B6", 1:5, sep="_"),
#		CH4_B7_1=paste("CH4_B7", 1:5, sep="_"),
#		CH4_B8_1=paste("CH4_B8", 1:5, sep="_"),
#		CH4_B9_1=paste("CH4_B9", 1:5, sep="_"), 
#		CH4_B10_1=paste("CH4_B10", 1:5, sep="_"), 
#		CH4_B11_1=paste("CH4_B11", 1:5, sep="_"),
#		CH4_B12_1=paste("CH4_B12", 1:5, sep="_"),
#		CH4_B13_1=paste("CH4_B13", 1:5, sep="_"),
#		CH4_R1_1=paste("CH4_R1", 1:5, sep="_"), 
#		CH4_R2_1=paste("CH4_R2", 1:5, sep="_"),
#		CH4_R3_1=paste("CH4_R3", 1:5, sep="_"),
#		CH4_R4_1=paste("CH4_R4", 1:5, sep="_"),
#		CH4_R5_1=paste("CH4_R5", 1:5, sep="_"),
#		CH4_R6_1=paste("CH4_R6", 1:5, sep="_"),
#		CH4_R7_1=paste("CH4_R7", 1:5, sep="_"),
#		CH4_W1_1=paste("CH4_W1", 1:5, sep="_"), 
#		CH4_W2_1=paste("CH4_W2", 1:5, sep="_"),
#		CH4_W3_1=paste("CH4_W3", 1:5, sep="_"),
#		CH4_W4_1=paste("CH4_W4", 1:5, sep="_"),
#		CH4_W5_1=paste("CH4_W5", 1:5, sep="_"),
#		CH4_W6_1=paste("CH4_W6", 1:5, sep="_"),
#		CH4_W7_1=paste("CH4_W7", 1:5, sep="_"),
#		CH4_W8_1=paste("CH4_W8", 1:5, sep="_"),
#		CH4_W9_1=paste("CH4_W9", 1:5, sep="_"), 
#		CH4_W10_1=paste("CH4_W10", 1:5, sep="_"), 
#		CH4_W11_1=paste("CH4_W11", 1:5, sep="_"),
#		CH4_W12_1=paste("CH4_W12", 1:5, sep="_"),
#		CH4_W13_1=paste("CH4_W13", 1:5, sep="_"), 
#		CH4_WAN_1=paste("CH4_WAN", 1:5, sep="_"), 
#		CH4_TER_1=paste("CH4_TER", 1:5, sep="_"), 
#		CH4_OCE_1=paste("CH4_OCE", 1:5, sep="_"), 
#		CH4_VOL_1=paste("CH4_VOL", 1:5, sep="_")), 
#	drop=paste0("MCF_", 1:5, "           Tr"))
#
#part.new = tmp$part
#hdr.new = tmp$hdr
#
##	write modified header and particles
#write.FLEXPART_header(hdr.new, file.path(path.out, "header"))
#write.particle.dump(part.new, file.path(path.out, "partposit_end"), is.ctm=TRUE)



