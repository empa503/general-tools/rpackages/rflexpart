#' Simulate VMR time series
#'	
#' VMR time series are simulated from total footprints for a given period of simulations.
#'
#' @export simulate.vmr.ts.from.total
simulate.vmr.ts.from.total = function(flex.dir, id, rel.com, domain="main", type="ncdf_fptot", 
	X, dtm.start, dtm.end, dt=3/24., 
	lev = 1, E, mu=28.0101, TT=293.15, PP=101325, rho=NULL, month.profile, 
	Interactive=TRUE){

    if (missing(E)) stop("Missing emisions field")
	if (missing(flex.dir)) stop("Missing total footprint directory: flex.dir")
	if (missing(id)) stop("Missing FLEXPART run identifier: id")


	if (missing(X)){
		dtm = seq.dates(dtm.start, dtm.end, by=dt)
		X = data.frame(dtm.start=dtm[-length(dtm)], dtm.end=dtm[-1])
	}
	
	tmp = assign.footprint.files(flex.dir=flex.dir, id=id, X=X, type=type, domain=domain)

	#	TODO for consisteny?

	X = tmp$X[[1]]
	flex.fls = tmp$flex.fls[[1]]
	flex.dtm = round(tmp$flex.dtm[[1]], "secs")
	rm(tmp)

	if (length(flex.fls)==0){
		cat("No associated FLEXPART simulations found.\n")
		return(NULL)
	}

	#	check what kind of emisisons where passed
	#########################################################################################
    if ("xrng" %in% names(E)){	#	simple emission field (one species, one domain)
        nn.E = 1
        E.main = list(VMR=E)
		E.nest = NULL
    } else if ("main" %in% names(E) && "nest" %in% names(E)){	#	two domains
		E.main = E[["main"]]
		E.nest = E[["nest"]]
		if ("xrng" %in% names(E.main)){
			E.main = list(VMR=E.main)
			E.nest = list(VMR=E.nest)
			nn.E = 1
		} else {
			nn.E = length(E.main)

			if (length(E.nest)!=nn.E) {
				stop("Emissions for nested and main domain contain different number of species")
			}			
        	if (length(mu)!=nn.E){
            	mu = rep(mu, length.out=nn.E)
	        }
		}		
	} else {	# list of emissions for one domain
        nn.E = length(E)
        if (length(mu)!=nn.E){
            mu = rep(mu, length.out=nn.E)
        }
		E.main = E
		E.nest = NULL
    }

	#	If merged domain is used, cut out nested emissions from main emissions to avoid double 
	#	countint. If necessary also edges of nested emissions are clipped. 
	if (!is.null(E.nest)){
		xrng.main = E.main[[1]]$xrng
		yrng.main =	E.main[[1]]$yrng
		nx.main = dim(E.main[[1]]$zz)[1]
		ny.main = dim(E.main[[1]]$zz)[2]
		dx.main = diff(xrng.main)/(nx.main-1)
		dy.main = diff(yrng.main)/(ny.main-1)
		if (length(dim(E.main[[1]]$zz))==3){
			nz = dim(E.main[[1]]$zz)[3]
		} else {
			nz = 0
		}

		xrng.nest = E.nest[[1]]$xrng
		yrng.nest =	E.nest[[1]]$yrng
		dx.nest = diff(xrng.nest)/(dim(E.nest[[1]]$zz)[1]-1)
		dy.nest = diff(yrng.nest)/(dim(E.nest[[1]]$zz)[2]-1)
		
		#	remove emissions within nested domain from main emissions
   	    ix = (xrng.nest - xrng.main[1])/dx.main + 1
       	jy = (yrng.nest - yrng.main[1])/dy.main + 1 

		#	check if nest outside main
		if (all(ix<1) || all(ix>=nx.main) || all(jy<1) || all(jy>ny.main)) {
			#	grid completely outside main domain
			
		} else { 
			if (ix[1]<1) ix[1] = 1
			if (ix[2]>=nx.main) ix[2] = nx.main
			if (jy[1]<1) jy[1] = 1
			if (jy[2]>=ny.main) iy[2] = ny.main

   		    ix = c(ceiling(ix[1]), trunc(ix[2]))
        	jy = c(ceiling(jy[1]), trunc(jy[2]))
	        xrng.nest.new = (ix-1)*dx.main + xrng.main[1]
   		    yrng.nest.new = (jy-1)*dy.main + yrng.main[1]

  			ix.nest = round((xrng.nest.new - xrng.nest[1])/dx.nest + 1)
   	        jy.nest = round((yrng.nest.new - yrng.nest[1])/dy.nest + 1)

			for (ii in 1:length(E.main)){
        		E.main[[ii]]$zz[ix[1]:ix[2],jy[1]:jy[2]] = 0.
				if (ix.nest[1]!=1 || ix.nest[2]!=hdr.nest$numxgrid){
					E.nest[[ii]]$zz[-(ix.nest[1]:ix.nest[2]),] = 0.
				}
				if (jy.nest[1]!=1 || jy.nest[2]!=hdr.nest$numygrid){
					E.nest[[ii]]$zz[, -(jy.nest[1]:jy.nest[2])] = 0.
				}
			}
		}
	}

	#	check that emissions are available for requested domains
	#########################################################################################
	if (domain=="merged"){
		if (is.null(E.nest)){
			stop("If merged domain is requested emissions need to be supplied for both domains.")
		}
	}
	if (domain=="main" && !is.null(E.nest)){
		warning("Main domain requested, emission for nest not used.")
		E.nest = NULL
	}
	if (domain=="nest" && !is.null(E.nest)){
		warning("Nested domain requested, emissions for main domain not used.")
		E.main = E.nest
		E.nest = NULL
	}
	
	#	create data.frame to hold results; each species gets separate column
	#########################################################################################
	vmr.spec = names(E.main)
	for (ii in 1:length(vmr.spec)) {
		X[[vmr.spec[ii]]] = 0
	}
	vmr.cols = which(names(X) %in% vmr.spec)

	#	column for counting footprints per time interval
	X$nn = 0 

	
	#	loop over all footprints
	###########################################################################################
	first.it = TRUE
	hdr = NULL	#	header information; used when reading from monthly netcdf
	cat("Calculating VMRs for", rel.com, ",", domain, "domain\n")
	if (Interactive) pb = txtProgressBar(min=1, max=length(flex.fls), initial=1, style=3)
	for (ii in  1:length(flex.fls)){

		if (Interactive) setTxtProgressBar(pb,ii)	

		#	load footprint
		if (type=="bin"){

			ft = footprint.read.bin(
				ifelse(domain=="merged", gsub("nest", "main", flex.fls[ii]), flex.fls[ii]),
					flip=FALSE)

		} else if (type=="ncdf_monthly"){
			#	hdr variable is used to keep track of previously opened file 
			#	and times contained therein 
			#	Alternatively, detection of need to open new file could be 
			#	handled through flex.fls vector. 
			if (!is.null(hdr)){ #	indicates that a ncdf file is already open
				tidx = which(hdr$time==flex.dtm[ii])
				#	if no match, most likely next file needs to be read
				#	Open ncdf file should be closed before opening new one. 
				if (length(tidx)!=1) {
					tidx = NULL
					hdr = NULL
					nc_close(cn)	#	close currently open file
					cn = NULL
				}
			} else {	#	indicates need to open new file
				tidx = NULL
				cn = NULL
			}
			#	reading from monthly sensitivity file
			#	When calling function with cn.in=NULL, it will keep netcdf file open.
			#	On return element 'cn' will contain the netcdf file handle.
			#	In a previous call this handle is submitted again to cn.in. 
			#	If reading of new file starts cn is closed before this call (see above)
			#	and cn.in will be set to NULL in the call. 
			ft = read.from.sensitivity.container(flex.fls[ii], tidx=tidx, rel.end=flex.dtm[ii], 
				hdr=hdr, cn.in=cn)
			#	Copy header and file handle. 
			if (is.null(hdr)) {
				hdr = ft.cur$hdr
				cn = ft.cur$cn
			}
			
		} else {

			if (missing(rel.com)) stop("Missing FLEXPART release identifier: rel.com")
			if (grepl("grid_time", basename(flex.fls[ii]))){
				main.suffix = ""
				spec = "fptot"
			} else {
				main.suffix = "_main"
				spec = 1
			}
			ft = read.grid.output.ncdf( 
				ifelse(domain=="merged", gsub("_nest", main.suffix, flex.fls[ii]), flex.fls[ii]),
				light=FALSE, levels = lev, spec=spec, release=rel.com, rel.end=flex.dtm[ii]) 

			if (any(ft$conc>1E35, na.rm=TRUE)) {
				warning("Some unrealistc values in file:", flex.fls[ii], "Skipping it.")
				ft = NULL
			}
		} 

		#	nested footprint
		if (!is.null(E.nest)){
			if (type=="bin"){
				ft.nest = footprint.read.bin(flex.fls[ii], flip=FALSE)
			} else {
				ft.nest = read.grid.output.ncdf(flex.fls[ii], 
					light=TRUE, levels = lev, spec=spec, release=rel.com, rel.end=flex.dtm[ii]) 
				if (any(ft.nest$conc>1E35, na.rm=TRUE)) {
					warning("Some unrealistc values in file:", flex.fls[ii], "Skipping it.")
					ft.nest = NULL
				}
			}
		}
		
		#	skip if NULL footprints returned
		############################################
		if (is.null(ft)) next	
		if (!is.null(E.nest) && is.null(ft.nest)) next

		#	check footprint/emission consistency
		###############################################
		if (any(abs(E.main[[1]]$xrng - ft$xrng)>0.01) ||
			any(abs(E.main[[1]]$yrng - ft$yrng)>0.01) ||
			any(dim(E.main[[1]]$zz)[1:2] != dim(ft$conc)[1:2])){
				
			stop(paste("Emission and sensitivities not on same grid.", 
				"Check output for:", chron.2.string(flex.dtm[ii])))
		}
		if (!is.null(E.nest)){
			if (any(abs(E.nest[[1]]$xrng - ft.nest$xrng)>0.01) ||
				any(abs(E.nest[[1]]$yrng - ft.nest$yrng)>0.01) ||
				any(dim(E.nest[[1]]$zz)[1:2] != dim(ft.nest$conc)[1:2])){
		
				stop("Nested emission and sensitivities not on same grid.")
			}
		}

		#	get surface area during first iteration
		############################################
		if (first.it){
			A = get.surface.area(grid=E.main[[1]], flip=FALSE, units="m^2")
			if (!is.null(E.nest)){
				A.nest = get.surface.area(grid=E.nest[[1]], flip=FALSE, units="m^2")
			}
			first.it = FALSE
		}

		#	calculate mixing ratios
		###################################################

		#	get index in time series
		idx = which(X$flex.first>=ii & X$flex.last<=ii)

		
		#	loop over all emission fields

		#################################################################
		#	simple for loop version
#		for (jj in 1:length(E.main)){
#			vmr = get.vmr(ft=ft, E=E.main[[jj]], A=A, lev=lev, mu=mu[jj],
#				TT=TT, PP=PP, rho=rho)
#			if (!is.null(E.nest)){
#				vmr.nest = get.vmr(ft=ft.nest, E=E.nest[[jj]], A=A.nest, lev=lev, mu=mu[jj],
#					TT=TT, PP=PP, rho=rho)
#			} else {
#				vmr.nest = 0
#			}
#			X[[vmr.spec[jj]]][idx] = X[[vmr.spec[jj]]][idx] + vmr + vmr.nest
#		}
		#################################################################

		#################################################################
		#	alternative version using mapply
		vmr = mapply(get.vmr, E=E.main, mu=mu, MoreArgs=list(ft=ft, TT=TT, PP=PP, 
			rho=rho, lev=lev, A=A)) 
		if (!is.null(E.nest)){
			vmr = vmr + 
				mapply(get.vmr, E=E.nest, mu=mu, MoreArgs=list(ft=ft.nest, TT=TT, PP=PP, 
					rho=rho, lev=lev, A=A.nest))
		}
		X[idx, vmr.cols] = X[idx, vmr.cols] + vmr
		#################################################################


		X$nn[idx] = X$nn[idx] + 1
	}	#	end of loop over footprints
	if (Interactive) close(pb)

	#	calculate means 
	#####################################
	msk = which(X$nn>0)
	for (jj in 1:length(E.main)){
		X[[vmr.spec[jj]]][msk] = X[[vmr.spec[jj]]][msk]/X$nn[msk]
		X[[vmr.spec[jj]]][-msk] = NA
	}
	X$nn = NULL
	X$flex.first = NULL
	X$flex.last = NULL

	return(X)
}



##	load emission files
#load("/nas/hes134/projects/CarboCountCH/emissions//E_main_JFJ.rda")
#load("/nas/hes134/projects/CarboCountCH/emissions//E_nest_JFJ.rda")
#mu = c(16, 44, 28)
#E = list(main=E.main, nest=E.nest)
#X = simulate.vmr.ts.from.total(flex.dir="/nas/output/FLEXPART_C7/total_footprint", 
#	id = "JFJ", rel.com="JUNGFRAUJOCH 3100",
#	type="ncdf_fptot", domain="merged", 
#	dtm.start = chron("2015-01-01", "00:00:00", format=c("y-m-d", "h:m:s")),
#	dtm.end = chron("2015-09-01", "00:00:00", format=c("y-m-d", "h:m:s")),
#	E = E, mu=mu
#	)
#
