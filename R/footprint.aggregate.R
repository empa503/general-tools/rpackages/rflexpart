#'	Extract and aggregate content of footprint. 
#'
#'	Allows to choose an individual release and aggregates over vertical levels and ageclasses
#'	at the same time. 
#	
#' @param aa		footprint representation, see 'read.grid.time' for structure
#' @param release	(integer)	individual release to be extracted
#' @param ageclass	(integer vector)	ageclasses to be aggregated
#' @param lev		(integer vector)	vertical levels to be aggregated
#' @param rotate	(logical)			matrix representation of aa$conc rotated?
#'
#' @return Returns modified copy of footprint aa
#' @author stephan.henne@@emap.ch
#' @export 
footprint.aggregate = function(aa, release=1, ageclass, lev, rotate=FALSE){

	if (missing(ageclass)){
		ageclass = 1:aa$hdr$nageclass
	} 

	if (missing(lev)){
		lev = 1:aa$hdr$numzgrid
	} 

	nn.lev = length(lev)
	nn.age = length(ageclass)
	nn.rel = length(release)
	nn.x = aa$hdr$numxgrid
	nn.y = aa$hdr$numygrid
	
	if (rotate){
		tmp = aa$conc[release, ageclass, lev,,]
		dim(tmp) = c(nn.rel, nn.age, nn.lev, nn.y, nn.x)
		tmp = apply(tmp, c(1,2,3), sum, na.rm=TRUE)
	} else {
		tmp = aa$conc[,,lev, release, ageclass]
		dim(tmp) = c(nn.x, nn.y, nn.lev, nn.age, nn.rel)
		tmp = apply(tmp, c(1,2), sum, na.rm=TRUE)
	}	

	aa$conc = tmp	
	
	#	adjust header information
	#######################################################
	hdr = aa$hdr
	
	#	release information
	hdr$rel.com = hdr$rel.com[release]
	hdr$rel.end = hdr$rel.end[release]
	hdr$rel.lat1 = hdr$rel.lat1[release]
	hdr$rel.lat2 = hdr$rel.lat2[release]
	hdr$rel.lng1 = hdr$rel.lng1[release]
	hdr$rel.lng2 = hdr$rel.lng2[release]
	hdr$rel.mass = hdr$rel.mass[,release]	
	hdr$rel.npart = hdr$rel.npart[release]
	hdr$rel.start = hdr$rel.start[release]
	hdr$rel.zagl1 = hdr$rel.zagl1[release]
	hdr$rel.zagl2 = hdr$rel.zagl2[release]
	hdr$rel.zasl1 = hdr$rel.zasl1[release]
	hdr$rel.zasl2 = hdr$rel.zasl2[release]
	hdr$rel.z.cor = hdr$rel.z.cor[release]
	hdr$numpoint = length(release)
	
	#	ageclass information
	hdr$lage = hdr$lage[ageclass][which.max(abs(hdr$lage[ageclass]))]
	hdr$nage = length(hdr$lage)

	#	level information
	if ("outheight" %in% names(hdr)){
		hdr$outheight = max(hdr$outheight[lev])
		hdr$numzgrid = length(hdr$outheight)
	}
	if ("zrng" %in% names(aa)){
		aa$zrng = max(aa$zrng[lev])
	}

	aa$hdr = hdr

	return(invisible(aa))
}

#test = footprint.aggregate(ft, release=1, lev=1:3)
