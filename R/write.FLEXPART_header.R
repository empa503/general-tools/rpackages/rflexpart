#	only an alias
#' @export
write.header = function(...) write.FLEXPART_header(...)
#	
#' @export
write.FLEXPART_header = function(hdr, fn, write.topo=FALSE, verbose=FALSE){

	if (missing(hdr)){
		stop("No header information to save")
	}
	if (missing(fn)){
		stop("No file name to save header information")
	}

	FORT.stop = as.raw(c(12,0,0,0))
	string.stop = as.raw(c(0,0))

	date = as.integer(format(as.POSIXlt(hdr$dtm0, "GMT"), "%Y%m%d"))
	time = as.integer(format(as.POSIXlt(hdr$dtm0, "GMT"), "%H%M%S"))

    strm = file(fn, open="wb")

	FORT.stop = as.raw(c(8 + nchar(hdr$model),0,0,0))
	writeBin(FORT.stop, strm, size=1)
    writeBin(date, strm, size=4)
    writeBin(time, strm, size=4)
	writeBin(paste(hdr$model, "\031", sep=""), strm)
	writeBin(string.stop, strm, size=1)

	FORT.stop = as.raw(c(4*3,0,0,0))
	writeBin(FORT.stop, strm, size=1)
	writeBin(as.integer(c(hdr$loutstep, hdr$loutaver, hdr$loutsample)), strm, size=4)
	writeBin(FORT.stop, strm, size=1)

	FORT.stop = as.raw(c(4*ifelse(hdr$is.cosmo,8,6),0,0,0))
	writeBin(FORT.stop, strm, size=1)
	writeBin(hdr$outlon0, strm, size=4)
	writeBin(hdr$outlat0, strm, size=4)
	writeBin(as.integer(hdr$numxgrid), strm, size=4)
	writeBin(as.integer(hdr$numygrid), strm, size=4)
	writeBin(hdr$dxout, strm, size=4)
	writeBin(hdr$dyout, strm, size=4)
	if (hdr$is.cosmo){
		writeBin(hdr$p0lon, strm, size=4)
		writeBin(hdr$p0lat, strm, size=4)
	}
	writeBin(FORT.stop, strm, size=1)

	FORT.stop = as.raw(c(4*(length(hdr$outheight)+1),0,0,0))
	writeBin(FORT.stop, strm, size=1)
	writeBin(as.integer(hdr$numzgrid), strm, size=4)
	writeBin(hdr$outheight, strm, size=4)
	writeBin(FORT.stop, strm, size=1)

	FORT.stop = as.raw(c(4*2,0,0,0))
	writeBin(FORT.stop, strm, size=1)
    writeBin(date, strm, size=4)
    writeBin(time, strm, size=4)
	writeBin(FORT.stop, strm, size=1)

	if (hdr$is.cosmo){
		FORT.stop = as.raw(c(4*2,0,0,0))
		writeBin(FORT.stop, strm, size=1)
    	writeBin(as.integer(99999999), strm, size=4)
	    writeBin(as.integer(999999), strm, size=4)
		writeBin(FORT.stop, strm, size=1)
	}

	FORT.stop = as.raw(c(4*2,0,0,0))
	writeBin(FORT.stop, strm, size=1)
    writeBin(as.integer(hdr$nspec*3), strm, size=4)
    writeBin(as.integer(hdr$maxpointspec_act), strm, size=4)
	writeBin(FORT.stop, strm, size=1)

	for (ii in 1:hdr$nspec){
		FORT.stop = as.raw(c(14,0,0,0))
		writeBin(FORT.stop, strm, size=1)
		writeBin(as.integer(1), strm, size=4)
		writeBin(paste0("WD_", substr(hdr$species.name[ii],1,7), "\016"), strm)
		writeBin(string.stop, strm, size=1)
		
		writeBin(FORT.stop, strm, size=1)
		writeBin(as.integer(1), strm, size=4)
		writeBin(paste("DD_", substr(hdr$species.name[ii],1,7), "\016", sep=""), strm)
		writeBin(string.stop, strm, size=1)

		FORT.stop = as.raw(c(18+4,0,0,0))
		writeBin(FORT.stop, strm, size=1)
		writeBin(as.integer(hdr$numzgrid), strm, size=4)
		writeBin(paste(sprintf("%-18s", hdr$species.name[ii]), "\026", sep=""), strm)
		writeBin(string.stop, strm, size=1)
	
		if (hdr$is.cosmo){
			FORT.stop = as.raw(c(15*4,0,0,0))
			writeBin(FORT.stop, strm, size=1)
			writeBin(rep(99., 12), strm, size=4)
			writeBin(as.integer(99), strm, size=4)
			writeBin(rep(99., 2), strm, size=4)
			writeBin(FORT.stop, strm, size=1)
		}
	}

	FORT.stop = as.raw(c(1*4,0,0,0))
	writeBin(FORT.stop, strm, size=1)
	writeBin(as.integer(hdr$numpoint), strm, size=4)
	writeBin(FORT.stop, strm, size=1)
		
	for (ii in 1:hdr$numpoint){
		FORT.stop = as.raw(c(2*4+2,0,0,0))
		writeBin(FORT.stop, strm, size=1)
		writeBin(as.integer((hdr$rel.start[ii] - hdr$dtm0) * 86400), strm, size=4)
		writeBin(as.integer((hdr$rel.end[ii] - hdr$dtm0) * 86400), strm, size=4)
		if (hdr$is.cosmo){
			writeBin(as.integer(hdr$rel.z.cor[ii]), strm)
		} else {
			writeBin(as.integer(hdr$rel.z.cor[ii]), strm, size=2)
		}
		writeBin(FORT.stop, strm, size=1)

		FORT.stop = as.raw(c(6*4,0,0,0))
		writeBin(FORT.stop, strm, size=1)
		tmp = c(hdr$rel.lng1[ii], hdr$rel.lat1[ii], hdr$rel.lng2[ii], hdr$rel.lat2[ii])
		if (hdr$rel.z.cor[ii]==1){
			tmp = c(tmp, hdr$rel.zagl1[ii], hdr$rel.zagl2[ii])
		} else {
			tmp = c(tmp, hdr$rel.zasl1[ii], hdr$rel.zasl2[ii])
		}
		writeBin(tmp, strm, size=4)
		writeBin(FORT.stop, strm, size=1)

		FORT.stop = as.raw(c(2*4,0,0,0))
		writeBin(FORT.stop, strm, size=1)
		writeBin(as.integer(hdr$rel.npart[ii]), strm, size=4)
		writeBin(as.integer(1), strm, size=4)
		writeBin(FORT.stop, strm, size=1)

		FORT.stop = as.raw(c(45,0,0,0))
		writeBin(FORT.stop, strm, size=1)
		writeBin(paste0(sprintf("%-45s", hdr$rel.com[ii]), "\055"), strm)
		writeBin(string.stop, strm, size=1)

		for (jj in 1:hdr$nspec){
			FORT.stop = as.raw(c(4,0,0,0))
			writeBin(FORT.stop, strm, size=1)
			writeBin(hdr$rel.mass[jj,ii], strm, size=4)
			writeBin(FORT.stop, strm, size=1)

			FORT.stop = as.raw(c(4,0,0,0))
			writeBin(FORT.stop, strm, size=1)
			writeBin(hdr$rel.mass[jj,ii], strm, size=4)
			writeBin(FORT.stop, strm, size=1)

			FORT.stop = as.raw(c(4,0,0,0))
			writeBin(FORT.stop, strm, size=1)
			writeBin(hdr$rel.mass[jj,ii], strm, size=4)
			writeBin(FORT.stop, strm, size=1)
		}
	}

	FORT.stop = as.raw(c(4*5,0,0,0))
	writeBin(FORT.stop, strm, size=1)
	tmp = c(hdr$method, hdr$lsubgrid, hdr$lconvection, hdr$ind_source, hdr$ind_receptor)	
	writeBin(as.integer(tmp), strm, size=4)
	writeBin(FORT.stop, strm, size=1)

	FORT.stop = as.raw(c(4*(1+length(hdr$lage)),0,0,0))
	writeBin(FORT.stop, strm, size=1)
	writeBin(as.integer(hdr$nageclass), strm, size=4)
	writeBin(as.integer(hdr$lage), strm, size=4)
	writeBin(FORT.stop, strm, size=1)
		
	if (write.topo){
		for (ii in 1:hdr$numxgrid){
			writeBin(FORT.stop, strm, size=1)
			writeBin(hdr$topo$zz[ii,], strm, size=4) 
			writeBin(FORT.stop, strm, size=1)
		}
	}

	close(strm)

	return(invisible(NULL))
}

#hdr = read.header("/nas/output/FLEXPART90/JFJ/2012/20120214/header")

#write.header("header_test", hdr)

#hdr.new = read.header("header_test", verbose=TRUE)

