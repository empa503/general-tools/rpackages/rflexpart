#' @export
read.initial.boundary.sensitivity.ncdf = function(fn, rel.com){

	if (!file.exists(fn)) {
		cat("File with boundary sensitivity does not exist.", fn, "\n")
		return(NULL)
	}
	if (missing(rel.com)) stop("Have to provide release name (rel.com)")

	cn = nc_open(fn)

	edge = list()
	edge$lon = as.vector(ncvar_get(cn, "lon"))
	edge$lat = as.vector(ncvar_get(cn, "lat"))
	edge$height = as.vector(ncvar_get(cn, "height"))
	edge$time = round(as.chron(as.vector(ncvar_get(cn, "time")), 
		out.format=c("y-m-d", "h:m:s")), "mins")
	rel.com.infile = as.vector(ncvar_get(cn, "RELCOM"))
	ridx = which(rel.com.infile == rel.com)

	if (length(ridx)!=1) {
		cat("Release not in file. Check rel.com.", "\n")
		return(NULL)
	}

	edge$north = ncvar_get(cn, "particle_locations_n", start=c(1,ridx,1,1), count=c(-1,1,-1,-1))
	edge$east  = ncvar_get(cn, "particle_locations_e", start=c(1,ridx,1,1), count=c(-1,1,-1,-1))
	edge$south = ncvar_get(cn, "particle_locations_s", start=c(1,ridx,1,1), count=c(-1,1,-1,-1))
	edge$west  = ncvar_get(cn, "particle_locations_w", start=c(1,ridx,1,1), count=c(-1,1,-1,-1))

	edge$age.north = ncvar_get(cn, "mean_age_particles_n", start=c(1,ridx,1,1), count=c(-1,1,-1,-1))
	edge$age.east  = ncvar_get(cn, "mean_age_particles_e", start=c(1,ridx,1,1), count=c(-1,1,-1,-1))
	edge$age.south = ncvar_get(cn, "mean_age_particles_s", start=c(1,ridx,1,1), count=c(-1,1,-1,-1))
	edge$age.west  = ncvar_get(cn, "mean_age_particles_w", start=c(1,ridx,1,1), count=c(-1,1,-1,-1))

	nc_close(cn)

	#	get total fraction of particles that left domain
	total = apply(edge$north, 1, sum) + 
			apply(edge$south, 1, sum) +
			apply(edge$east, 1, sum) +
			apply(edge$west, 1, sum) 


	#	remaining fraction of particles in domain
	edge$frac.in = 1. - total

	#	normalise so that edge fractions add up to 1
	tmp = array(rep(total, prod(dim(edge$north[2:3]))), dim=dim(edge$north))
	edge$north = edge$north/tmp
	edge$south = edge$south/tmp
	tmp = array(rep(total, prod(dim(edge$west[2:3]))), dim=dim(edge$west))
	edge$west = edge$west/tmp
	edge$east = edge$east/tmp

	return(edge)
}

