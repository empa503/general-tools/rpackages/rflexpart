
write.RECEPTORS = function(fn="RECEPTORS", rec=NULL, version=c("9.1", "COSMO")){

	version = match.arg(version)
	

	strm = file(fn, open="w")

	#	write header 
	cat("********************************************************************************\n", file=strm)
	cat("*                                                                              *\n", file=strm)
	cat("*      Input file for the Lagrangian particle dispersion model FLEXPART        *\n", file=strm)
	cat("*                       Please specify your receptor points                    *\n", file=strm)
	cat("*     For the receptor points, ground level concentrations are calculated      *\n", file=strm)
	cat("*                                                                              *\n", file=strm)
	cat("********************************************************************************\n", file=strm)

	#	write individual receptors
	if (!is.null(rec)){
		nn.rec = nrow(rec) 
		for (ii in nn.rec){
			cat("1.  ----------------  4X,A16\n", file=strm) 
			cat("   ", sprintf("%-16s", rec$name[ii]), "NAME OF RECEPTOR POINT\n", file=strm) 
			cat("    RECEPTORNAME\n", file=strm)
			cat("\n", file=strm)
	
			cat("2.  ------.----       4X,F11.4\n", file=strm)
			cat("   ", sprintf("%11.4f", rec$lon[ii]), "GEOGRAFICAL LONGITUDE\n", file=strm)
			cat("    XRECEPTOR\n", file=strm)
			cat("\n", file=strm)

			cat("3.  ------.----       4X,F11.4\n", file=strm)
			cat("   ", sprintf("%11.4f", rec$lat[ii]), "GEOGRAFICAL LATITUDE\n", file=strm)
			cat("    YRECEPTOR\n", file=strm)
			cat("\n", file=strm)

			cat("================================================================================\n", file=strm)
		}
	}
	close(strm)

}
