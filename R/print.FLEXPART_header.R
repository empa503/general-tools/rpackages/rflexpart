#' @export print.FLEXPART_header
print.FLEXPART_header = function(hdr){
    cat("FLEXPART header:\n")
    cat("Version: ", hdr$model, "\n")
    cat("Start of simulation: ", format(as.POSIXlt(hdr$dtm, "GMT"), "%Y-%m-%d %H:%M:%S"), "\n")
    cat("\nOUTGRID\n")
    cat("nx: ", hdr$numxgrid, " ny: ", hdr$numygrid, " nz: ", hdr$numzgrid, "\n")
    cat("Longitudes: ", hdr$outlon0, " - ", hdr$outlon0+hdr$dx*(hdr$numxgrid-1), "(deg E), dx:", hdr$dx, "\n")
    cat("Latitudes: ", hdr$outlat0, " - ", hdr$outlat0+hdr$dy*(hdr$numygrid-1), "(deg N), dy:", hdr$dy, "\n")
    cat("Top of vertical levels: ", paste(hdr$outheight, collapse=" "), " (m) above model ground \n")
    
    cat("\nRELEASES\n")
    for (ii in 1:hdr$numpoint){
        cat("Release ", ii, ": ", hdr$rel.com[ii], "\n")
        cat("Longitudes: ", hdr$rel.lng1[ii], " - ", hdr$rel.lng2[ii], "(deg E)\n")
        cat("Latitudes: ", hdr$rel.lat1[ii], " - ", hdr$rel.lat2[ii], "(deg N)\n")
        cat("Altitudes: ", hdr$rel.zasl1[ii], " - ", hdr$rel.zasl2[ii], "m above sea level", "\n")
        cat("Altitudes: ", hdr$rel.zagl1[ii], " - ", hdr$rel.zagl2[ii], "m above ground level", "\n")
        cat("Release start: ", format(as.POSIXlt(hdr$rel.start[ii], "GMT"), "%Y-%m-%d %H:%M:%S"), "\n")
        cat("Release end: ", format(as.POSIXlt(hdr$rel.end[ii], "GMT"), "%Y-%m-%d %H:%M:%S"), "\n")
    }
    cat("\nSPECIES\n")
    for (ii in 1:hdr$nspec){
        cat("Species ", ii, ": ", hdr$species.name[ii], "\n")
    }
    
    cat("Model settings\n")
    cat("Output sample ", hdr$loutsample, "(s)\n")
    cat("Output step   ", hdr$loutstep, "(s)\n")
    cat("Output average", hdr$loutaver, "(s)\n")
}
