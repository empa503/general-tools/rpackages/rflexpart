
get.emission.sensitivity.to.point.source = function(dtm.start, dtm.end, dt, flex.dir, id, rel.com, 
	domain, type, pnt.src){

	dtm = seq.dates(dtm.start, dtm.end, by=dt)
	X = data.frame(dtm.start=dtm[-length(dtm)], dtm.end=dtm[-1])
	tmp = assign.footprint.files(flex.dir=flex.dir, id=id, X=X, type=type, domain=domain)

	if (type=="bin"){
		fp = footprint.read.bin(tmp$flex.fls[[1]][1], flip=FALSE)
	} else {
		#	TODO
		fp = read.grid.output.ncdf(tmp$flex.fls[[1]][1], release=rel.com)
	}
	trgt = fp
	trgt$conc = NULL
	trgt$zrng = NULL
	trgt$dtm0 = NULL
	if (length(dim(fp$conc)>2)){
		trgt$zz = fp$conc[,,1,1,1]
	} else {
		trgt$zz = fp$conc
	}
	trgt$zz[,] = 0.

	E = vector("list", length(pnt.src))
	names(E) = names(pnt.src)
	for (ii in 1:length(pnt.src)){
		E[[ii]] = trgt

		ix = round((pnt.src[[ii]]$x - trgt$xrng[1])/trgt$dx) + 1
		jy = round((pnt.src[[ii]]$y - trgt$yrng[1])/trgt$dy) + 1

		E[[ii]]$zz[ix,jy] = 1E6 #	1E6 ug/s -> 1 g/s

	}

	#	get time series in ppb
	dat = simulate.vmr.ts.from.total(X=X, flex.dir=flex.dir, id=id, rel.com=rel.com, domain=domain,
		type=type, E=E, mu=28.964)
	#	units should then be ppb s g-1
	
	for (ii in 1:length(pnt.src)){
		dat[[names(pnt.src)[ii]]] = dat[[names(pnt.src)[ii]]] * 1000
		attr(dat[[names(pnt.src)[ii]]], "units", "ppt s g-1")
	}
	

	return(dat)	
}

#
#
