#
#	TODO not up to date with current structure of conc
#' @export
plot.grid.conc.by.ageclass = function(conc, para, zlim=NULL, lev=1, unit="ppbv", log.scale=TRUE, all.color.scales=FALSE, clock=1, ageclass, ...){
    if(length(dim(conc$conc))<5){
        warning("Concentration field must have 5 dimensions: x,y,z,clock,ageclass.")
        return(NULL)
    }
    if (!missing(ageclass)){
        conc$conc = conc$conc[,ageclass,,,]
    }else{
        ageclass = 1:dim(conc$conc)[2]
    }

    lage = c(-Inf, conc$hdr$lage, Inf) 
    age.rng = NA
    for (ii in 1:length(ageclass)){
        if (conc$hdr$lage[ageclass[ii]]>0) {
            age.rng[ii] = paste(round(lage[ageclass[ii]+1]/86400), "-", round(lage[ageclass[ii]+2]/86400), "days")
        } else if(conc$hdr$lage[ageclass[ii]]<0){
            age.rng[ii] = paste(round(lage[ageclass[ii]]/86400), "-", round(lage[ageclass[ii]+1]/86400), "days")
        } else {
            age.rng[ii] = "Not initialized"
        }
    }

    nn.age = dim(conc$conc)[2] 

    
    if (length(lev)==1) {
        if (lev==1){
            lev.rng = c(0, conc$zrng[lev])
        } else {
            lev.rng = c(conc$zrng[lev-1], conc$zrng[lev])
        }
    } else{
        if (lev[1]==1){
            lev.rng = c(0, conc$zrng[lev[length(lev)]])
        }else{
            lev.rng = c(conc$zrng[lev[1]-1], conc$zrng[lev[length(lev)]])
        }   
    }
    lev.rng = paste(lev.rng, collapse="-")
    key.title = as.expression(bquote(C[.(lev.rng)]*(.(unit))))

    if (length(zlim)!=nn.age){
        tmp = zlim
        zlim = vector("list", nn.age)
        if(!is.null(tmp)){
            for (ii in 1:nn.age){
                zlim[[ii]] = tmp
            }
        }
    }
    nn.col = ceiling(sqrt(nn.age))
    nn.row = ceiling((nn.age)/nn.col)

    w <- (3 + 2) * par("csi") * 2. 

    if (all.color.scales){
        layout(matrix(1:(nn.row*(2*nn.col)), ncol=2*nn.col, byrow=TRUE), widths=rep(c(1,lcm(w)),nn.col), heights=rep(1,nn.row))
    }else {
        layout(matrix(1:(nn.row*(nn.col+1)), ncol=nn.col+1, byrow=TRUE), widths=c(rep(1,nn.col), lcm(w)), heights=rep(1,nn.row))
    }
    kk = 1
    for (ii in 1:nn.age){
        color.palette = plot.grid.conc(conc, clock=clock, ageclass=ii, plot.key=FALSE, zlim=zlim[[ii]], mar=c(3,4,1,1)+.1, key.title=key.title, log.scale=log.scale, lev=lev, FUN=mean, ...)
        mtext(age.rng[ii], 3, 0, cex=.9*par("cex"))

        if (all.color.scales){
            plot.colorpalette(color.palette, las=3)
        } else {
            if((kk+1) %% (nn.col+1) == 0) {
                plot.new()
                kk = kk + 1
            }
        } 

        kk = kk + 1
    }
    if (!all.color.scales){
        #   plot color key
        for (kk in kk:((nn.col+1)*nn.row-1)){
            plot.new()
        }
        plot.colorpalette(color.palette, las=3)
    }
}
