#' @export
get.mean.vmr.xz = function(part, lev, ylim=NULL, na.rm=TRUE, release=1, age=1:part$hdr$nage, ar.FUN=sum){
	if (length(dim(part$conc))>3){
		part$conc = apply(part$conc[,,,release,age], 1:3, FUN=ar.FUN, na.rm=TRUE)
	}
	
    nx = dim(part$conc)[1]
    ny = dim(part$conc)[2]
    nz = dim(part$conc)[3]

    if (missing(lev)) lev = 1:nz

    if (!is.null(ylim)){
        dy = diff(part$yrng)/(ny-1)
        yy = seq(part$yrng[1], part$yrng[2], dy) 
        msk = which(yy>=ylim[1] & yy<=ylim[2])
        if (length(msk)==0){
            warning("Selected y-range not in grid. Using total instead.")
            msk = 1:ny
        }
    } else{
        msk = 1:ny
    }
		
    part$dx = diff(part$xrng)/(nx-1)
    part$dy = diff(part$yrng)/(ny-1)
    
	if (!"projection" %in% names(part)){
	    aa = get.surface.area(part$xrng+c(-0.5, 0.5)*part$dx, part$yrng+c(-0.5, 0.5)*part$dy, part$dx, part$dy, flip=FALSE)
    	aa = apply(aa$zz, 2, mean)[msk]
    
		cc = apply(part$conc[,msk,lev], c(1,3), weighted.mean, w=aa, na.rm=na.rm)
	} else {
		cc = apply(part$conc[,msk,lev], c(1,3), mean, na.rm=na.rm)
	}

#    cc = cc[nrow(cc):1, ]
    cc[cc==0] = NA
    zrng = part$zrng
    tot = list(xrng=seq(part$xrng[1], part$xrng[2], length.out=nx), zrng=zrng, conc=cc, hdr=part$hdr)

    msk = which(!names(part) %in% names(tot))
    if (length(msk)>0){
        for (ii in 1:length(msk)){
            tot[[names(part)[msk[ii]]]] = part[[names(part)[msk[ii]]]]
        }   
    }
    if ("zrng" %in% names(tot)) tot$zrng = tot$zrng[lev]
    if ("outheight" %in% names(tot$hdr)) tot$hdr$outheight = tot$hdr$outheight[lev]
    return(tot)
}

##
##	example
##
#require(Rflexpart)
##	load some footprint data
#                   
#ft = read.grid.output("C:/empadaten/hes134/projects/FLEXPART/example_output/forward/grid_conc_20100417100000_004")
##	calculate mean.vmr.xz
#vmr.xz = get.mean.vmr.xz(ft, ylim=c(60,70))
##	plot
#par(mfcol=c(1,2))
#plot.grid.conc(ft, lev=1:20, plot.key=FALSE, log.scale=FALSE)
#fill.grid(vmr.xz$xrng, vmr.xz$zrng, vmr.xz$conc, rotate=FALSE, plot.key=FALSE, color.palette=special.rainbow)
