#'	Interpolate 3D model fields to particle positions
#'	
#'	@param	prod.dir If not NULL individual plots of the fields to interpolate and the particle
#'						positions are created.
#'	
#' @export
partpos.get.baseline = function(part, hdr, gctm.FUN, gctm.args, gctm.fls, prod.dir=NULL, 
	bg.name, max.fields=50, get.sd=FALSE){
	
	#	analyse by release point
	###########################################
	s.part = split(part, f=factor(part$npoint))	
	for (ii in 1:length(s.part)){
		if (!is.null(s.part[[ii]]$itime)){
			s.part[[ii]]$dtm = hdr$rel.end[ii] + s.part[[ii]]$itime/86400 
		} else if (!is.null(s.part[[ii]]$tropo)){
			s.part[[ii]]$dtm = hdr$rel.end[ii] + s.part[[ii]]$tropo/86400 
		} else {
			stop("particle dump not compatible.")
		}
	}
	if (!is.null(prod.dir)){
		prod.dir = file.path(prod.dir, gsub(" ", "_", hdr$rel.com), 
			chron.2.string(hdr$rel.end, "%Y")) 
		for (ii in 1:length(prod.dir)){
			if (!dir.exists(prod.dir[ii])) dir.create(prod.dir[ii], recursive=TRUE)
		}	
		plot.fn = paste0(hdr$rel.com, "_", chron.2.string(hdr$rel.end, "%Y%m%d_%H"))
		plot.fn = file.path(prod.dir, plot.fn)
	} else {
		plot.fn = vector("list", length(hdr$rel.com))
	}

	#	check if order in s.part is unchanged
	ord = order(as.numeric(names(s.part)))

	#	apply the interpolation, takes care of loading required fields
	if (get.sd){
		cc = mapply(get.field.agg.at.points, s.part, plot.fn=plot.fn, 
			MoreArgs=list(agg.FUN=list(mean, sd), gctm.FUN=gctm.FUN, 
			gctm.args=gctm.args, gctm.fls=gctm.fls, max.fields=max.fields))
	} else {
		cc = mapply(get.field.agg.at.points, s.part, plot.fn=plot.fn, 
			MoreArgs=list(agg.FUN=mean, gctm.FUN=gctm.FUN, 
			gctm.args=gctm.args, gctm.fls=gctm.fls, max.fields=max.fields))
	}

	

	#	output of results
	###########################################

	#	create data.frame of current results
	res = data.frame(dtm=hdr$rel.end, rel.com=hdr$rel.com, stringsAsFactors=FALSE)
	if (get.sd){
		res[[bg.name]] = unlist(cc[1,ord])
		res[[paste0("u.", bg.name)]] = unlist(cc[2,ord])
	} else {
		res[[bg.name]] = cc[ord] 
	}

	return(res)
}

#'	Extract timeseries of interpolated 3D data to particle positions
#'
#'	For a given period and FLEXPART output the function will evaluate initial particle 
#'	positions and interpolate values of a 3D model to these positions, generate a mean 
#'	value for each release and store the results as ASCII files separately for each release 
#'	location. 
#'	Main work is done in routine 'partpos.get.baseline'.
#'	The function assumes daily FLEXPART output in <flex.dir>/<id>/yyyy/<day.frmt>
#'
#'  @param flex.dir Directory with FLEXPART raw output.
#'  @param ids FLEXPART run identifiers (not release name). Can be a vector of multiple IDs.
#'	@param dtm.start start of period to be processed (chron)
#'  @param dtm.end end of period to be processed (chron)
#'	@param gctm.FUN
#'  @param gctm.args 
#'  @param gctm.fls 
#'  @param day.frmt date/time format used for storing individual days of FLEXPART output
#'  @param prod.dir base directory where to store results in. Final output directory will be
#'				 <prod.dir>/<rel.com>/yyyy
#'	@param bg.name name that should be given to interpolated values. Default is 'C_bg'.
#'	@param overwrite	If TRUE all days are processed and any old results overwritten. 
#'						If FALSE (default) existing results are not changed and only days 
#'						with missing results are processed.
#'	@param part.field.plots If TRUE individual plots for each interpolated field and particle
#'						ensemble are produced, which results in a large number of plots
#'						This option is highly experimental since the plot options are hard 
#'						coded in 'myRtools::get.field.agg.at.points'.
#' @param max.fileds Maximal number of model fields held in memory. If too small fields may be 
#'						read repeatedly. If too large you may run out of RAM.
#' @param get.sd (logical) By default the function obtains the mean over all particle positions. 
#'						If 'get.sd' is set to TRUE, standard deviation is obtained in addition
#'						as a colum 'u.<bg.name>'. Default is get.sd=FALSE.
#'
#' @export
partpos.get.baseline.timeseries = function(flex.dir, ids, dtm.start, dtm.end, gctm.FUN, 
	gctm.args=NULL, gctm.fls, day.frmt="%Y%m%d00", prod.dir, bg.name="C_bg", overwrite=FALSE, 
	part.field.plots=FALSE, max.fields=50, get.sd=FALSE){

	dtm.cur = dtm.start

	#	time series files will start with the same prefix as the containing product folder
	prod.str = basename(prod.dir)

	#	setting a global variable for containing the fields
	gctm <<- list()

	#	time loop
	###########################################
	repeat{	
		#	break condition
		if (dtm.cur>dtm.end) break

		cat(chron.2.string(dtm.cur), "\n")

		#	loop over run IDs
		###########################################
		for (id in ids){		

			#	locate output
			################################################
			yyyy = chron.2.string(dtm.cur, "%Y")
			flex.dir.cur = file.path(flex.dir, id, yyyy, 
				chron.2.string(dtm.cur, day.frmt))
	
			#	first check if output for day available; if not check for tar file and extract
			################################################
			from.tar = FALSE
			if (!file.exists(flex.dir.cur)){
				if (file.exists(paste0(flex.dir.cur, ".tar"))){
					cmd = paste("tar xvf", paste0(flex.dir.cur, ".tar"), "-C", 
						dirname(flex.dir.cur))
					system(cmd)
					from.tar = TRUE
				}
			} 
	
			#	check if output for day available
			################################################
			if (!file.exists(flex.dir.cur)) {
				cat("FLEXPART output directory does not exist.")
				next
			}
	
			#	check and load FLEXPART header
			################################################
			hdr.fn = file.path(flex.dir.cur, "header") 
			if (!file.exists(hdr.fn)) {
				cat("Cannot find FLEXPART header file.")
				next
			}
			hdr = read.header(hdr.fn)
	
			#	check if results already exist, skip day if overwrite==FALSE
			################################################
			if (!overwrite){
				fn.csv = file.path(prod.dir, gsub(" ", "_", hdr$rel.com[length(hdr$rel.com)]), 
					yyyy, paste0(prod.str, "_", gsub(" ", "_", hdr$rel.com[length(hdr$rel.com)]), 
						"_", yyyy, ".csv"))
				if (file.exists(fn.csv)){
					X = readfile.csv(fn.csv)
					if (all(hdr$rel.end %in% X$dtm)){
						cat("Day already processed. Skipping.\n")
						next
					}
				}
			}
	
			#	check for offline nest; extend dir name
			################################################
	
			#	check if particle file exists
			################################################
			if (dir.exists(file.path(flex.dir.cur, "offline_nest"))){
				part.fn = file.path(flex.dir.cur, "offline_nest", "partposit_inst")
			} else {
				part.fn = file.path(flex.dir.cur, "partposit_inst")
			}	
			if (!file.exists(part.fn)) {
				part.fn = paste0(part.fn, ".gz")
				if (!file.exists(part.fn)) {
					cat("Cannot find FLEXPART header file.")
					next
				}
			}

			#	read particle positions
			###########################################
			cat("Reading particle dump ", part.fn, "...\n")
			part = read.particle.dump(part.fn)
			cat("done.\n")
	
			if (is.null(part)){
				cat("read.particle.dump returned NULL")	
				next
			}
			
			#	do the interpolation 
			###########################################
			if (part.field.plots){
				res = partpos.get.baseline(part=part, hdr=hdr, gctm.FUN=gctm.FUN, 
					gctm.args=gctm.args, gctm.fls=gctm.fls, bg.name=bg.name,
					prod.dir=prod.dir, max.fields=max.fields, get.sd=get.sd)
			} else {
				res = partpos.get.baseline(part=part, hdr=hdr, gctm.FUN=gctm.FUN, 
					gctm.args=gctm.args, gctm.fls=gctm.fls, bg.name=bg.name, prod.dir=NULL,
					max.fields=max.fields, get.sd=get.sd)
			}

	
			#	save results in separate files by release name
			####################################################
			s.res = split(res, f=factor(res$rel.com))
			for (ii in 1:length(s.res)){
				s.res[[ii]]$rel.com = NULL
	
				#	file to hold results
				fn.csv = file.path(prod.dir, gsub(" ", "_", names(s.res)[ii]), yyyy, 
					paste0(prod.str, "_", gsub(" ", "_", names(s.res)[ii]), "_", yyyy, ".csv"))
				#	if exists read and combine
				if (file.exists(fn.csv)){
					#	read
					X = readfile.csv(fn.csv)
					X$Source = NULL
	
					#	remove entries that are part of new results
					if (overwrite){
						msk = which(X$dtm %in% s.res[[ii]]$dtm)
						if (length(msk)>0) {
							X = X[-msk,]
						}
					} else {
						msk = which(s.res[[ii]]$dtm %in% X$dtm)
						if (length(msk)>0){
							s.res[[ii]] = s.res[[ii]][-msk,]
						}
					}
		
					#	merge with current results
					X = merge(X, s.res[[ii]], all=TRUE)
				} else {
					if (!file.exists(file.path(prod.dir, gsub(" ", "_", names(s.res)[ii]), yyyy))){
						dir.create(file.path(prod.dir, gsub(" ", "_", names(s.res)[ii]), yyyy), 
							recursive=TRUE)
					}
					#	new results
					X = s.res[[ii]]
				}
				#	sort by time
				X = X[order(X$dtm),]
	
				#	save file
				save.file(ffile=fn.csv, X=X)
			}
	
			#	tidy up if FLEXPART results were extracted from tar file
			if (from.tar){
				cmd = paste("rm -rf", flex.dir.cur)
				system(cmd)
			}

		} #	loop over run IDs

		#	time loop increment
		dtm.cur = dtm.cur + 1

	} #	end of time loop

	return(invisible(NULL))
}


##	EXAMPLE 
#
#require(Rflexpart)
#source("~/progs/R_misc/get.TM5.field.R")
#
#flex.dir = "/hnas/output/FLEXPART_C7"
#prod.dir = "/hnas/output/FLEXPART_C7_products/TM5_CH4_baseline"
#id = "LAE"
#dtm.start = chron("2013-04-01", "00:00:00", format=c("y-m-d", "h:m:s"))
#dtm.end   = chron("2013-05-01", "00:00:00", format=c("y-m-d", "h:m:s"))
#gctm.FUN = get.TM5.field
#gctm.fls = get.TM5.fls("/nas/input/TM5_4DVAR_GOS_OCPR_v6.0")
#gctm.args = list(para="CH4", type="dry")
#bg.name = "CH4_bg"
#overwrite = FALSE
#
#
#partpos.get.baseline.timeseries(flex.dir=flex.dir, id=id, dtm.start, dtm.end=dtm.end, 
#	gctm.FUN=gctm.FUN, gctm.args=gctm.args, gctm.fls=gctm.fls, prod.dir=prod.dir, 
#	bg.name=bg.name, overwrite=overwrite)

