#' A simple footprint plot
#' 
#' Produces a simple footprint plot without any axis annotations, color keys, borders, etc. 
#' The plot should be used to produce files that can be easily represented as GroundOverlays
#' in GoogleEarth.
#'
#' @param ft Footprint to be plotted.
#' @param print.info (logical) If TRUE information on release and sampling time are printed 
#'		in the upper left corner of the plot. Default is not to print. 
#' @param release (numeric) Release index to be plotted. Default is 1.
#' @param lev (numeric) Level to be plotted. Default is 1.
#' @param plot.key (logical) plot color key next to figure. Default is FALSE.
#' @param frame.plot (logical) plot box around figure region. Default is FALSE.
#' @param axes (logical) plot x and y axis. Default is FALSE
#' @param rotate (logical) Flip orientation of array that is to be printed. Default is FALSE.
#' @param key.title (character) Key title for color legend. Even if 'plot.key' is FALSE
#' 		the key title will be part of the color key that is returned by the function and 
#'		can be used in a separate plot.
#' @param log.scale (logical) Use logarithmic color key. Default is TRUE.
#' @param color.palette (function) Color ramp function for color key. Default is 'special.rainbow'.
#' @param palette.par (list) list of additional parameters passed to color.palette. 
#'		Default is alpha=.99 which together with special.rainbow assures a semi-transparent 
#'		color key that fades to full transparency for low values. Other color ramp functions
#'		might not support an alpha value!
#' @param ... Further parameters passed to \code{\link{fill.2dplot}} 
#'
#' @return A color key that can be plotted with \code{\link{plot.colorpalette}}
#'
#' @export 
grid.time.kmlplot = function(ft, print.info=FALSE, release=1, lev=1, plot.key=FALSE, 
	frame.plot=FALSE, axes=FALSE, rotate=FALSE, key.title="", log.scale=TRUE, 
	color.palette=special.rainbow, palette.par=list(alpha=.99), ...){

	par(mar=c(0,0,0,0), bg="transparent")
	if (length(dim(ft$conc))==5){
		ft$zz = ft$conc[,,lev,release,1]
	} else if (length(dim(ft$conc))==3){
		ft$zz = ft$conc[,,lev]
	} else {
		ft$zz = ft$conc
	}
	ft$zrng = NULL

	colscale = fill.2dplot(ft, plot.key=plot.key, frame.plot=frame.plot, 
		axes=axes, rotate=rotate, key.title=key.title, log.scale=log.scale, 
		color.palette=color.palette, palette.par = palette.par, ...)

    if (!is.null(ft$hdr) && print.info){
		#	release times
		rel.start = ft$hdr$rel.start[release]
		rel.end = ft$hdr$rel.end[release]
		if (!is.null(rel.start) && !is.null(rel.end)){
	        rel = paste(format(as.POSIXlt(rel.start, "GMT"), "%Y-%m-%d %H:%M"), 
    	        format(as.POSIXlt(rel.end, "GMT"), "%Y-%m-%d %H:%M"), sep=" - ")    
		} else {
			rel = ""
		}
		#	release comment
        rel.com = ft$hdr$rel.com[release]  
        mtext(paste("Release:", rel.com, rel), 3, -1, adj=0, font=1)
		
		#	residence time range
        if (!is.null(ft$dtm)){
			if (is.list(ft$dtm)){
				tau.start = ft$dtm[[release]][1]	
				tau.end = ft$dtm[[release]][2]	
			} else {
				tau.start = ft$dtm[1]
				tau.end = ft$dtm[2] 
			}

			if (!is.na(tau.start) && !is.na(tau.end)){
				if (ft$hdr$lage!=999999999 && length(ft$hdr$lage)==1){
					tau.rng = (tau.end - tau.start)*86400
					if(tau.rng>ft$hdr$lage){
						tau.start = rel.start - ft$hdr$lage/86400
						tau.end = rel.end
					}	
				}
				mtext(paste("Residence times:", paste(format(round(as.POSIXlt(c(tau.start,tau.end), 
					"GMT"), "min"), "%Y-%m-%d %H:%M"), collapse=" - ")), 3, -2, adj=0, font=1)
			}
		}
    }
	
	return(colscale)
}

