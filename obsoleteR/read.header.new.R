
#####################################################################################################
##               read.header
#####################################################################################################
##   purpose:    read header information of FLEXPART output
##   args:       fn      (character) file name of header file to read, if missing file choose dialog is opened
##               verbose (logical)   if TRUE, header information is printed while reading
##   returns:    (list)  of class FLEXPART_header. Needed as input for any reading of FLEXPART ouput grid.
##   author:     stephan.henne@empa.ch
##   version:    0.1-20051005
##   requires:   
#####################################################################################################
#read.header.new <- function(fn, verbose=FALSE){
#    library(chron)
#    
#    if(missing(fn)) fn = file.choose()
#    
#    strm = file(fn, open="rb")
#    
#    seek(strm, 4, origin="current")
#    date = readBin(strm, "integer", n=1)
#    if (verbose) cat("date:", date, "\n")
#    time = readBin(strm, "integer", n=1)
#    if (time==0) time = "000000"
#    if (as.numeric(time)<100000) time = paste("0", time, sep="")
#    if (verbose) cat("time:", time, "\n")
#    date = as.character(date)
#    dtm = chron(paste(substr(date, 1, 4), substr(date, 5, 6), substr(date, 7, 8)), as.character(time), format=c("y m d", "hms"), out.format=c("y-m-d", "h:m:s"))
#    if (verbose) cat(as.character(dtm), "\n")
#    model = readBin(strm, "byte", n=1)
#    if (verbose) cat("model:", model, "\n")
#    
#    seek(strm, 6, origin="current")
#    loutstep = readBin(strm, "integer", n=1)
#    if (verbose) cat("loutstep:", loutstep, "\n")    
#    loutaver = readBin(strm, "integer", n=1)
#    if (verbose) cat("loutaver:", loutaver, "\n")        
#    loutsample = readBin(strm, "integer", n=1)
#    if (verbose) cat("loutsample:", loutsample, "\n")    
#    
#    seek(strm, 8, origin="current")    
#
#    outlon0 = readBin(strm, "numeric", size=4, n=1)        
#    if (verbose) cat("outlon0:", outlon0, "\n")    
#    outlat0 = readBin(strm, "numeric", size=4, n=1)        
#    if (verbose) cat("outlat0:", outlat0, "\n")    
#    numxgrid = readBin(strm, "integer", n=1)    
#    if (verbose) cat("numxgrid:", numxgrid, "\n")    
#    numygrid = readBin(strm, "integer", n=1)    
#    if (verbose) cat("numygrid:", numygrid, "\n")    
#    dxout = readBin(strm, "numeric", size=4, n=1)    
#    if (verbose) cat("dxout:", dxout, "\n")    
#    dyout = readBin(strm, "numeric", size=4, n=1)    
#    if (verbose) cat("dyout:", dyout, "\n")    
#    
#    seek(strm, 8, origin="current")    
#
#    numzgrid = readBin(strm, "integer", n=1)
#    if (verbose) cat("numzgrid:", numzgrid, "\n")    
#    outheight = readBin(strm, "numeric", size=4, n=numzgrid)
#    if (verbose) cat("outheight:", outheight, "\n")        
#
#    seek(strm, 8, origin="current")    
#
#    jjjjmmdd = readBin(strm, "integer", n=1)
#    if (verbose) cat("jjjjmmdd:", jjjjmmdd, "\n")    
#    ihmmss = readBin(strm, "integer", n=1)
#    if (verbose) cat("ihmmss:", ihmmss, "\n")    
#        
#    seek(strm, 8, origin="current")    
#    
#    nspec = readBin(strm, "integer", n=1)/3
#    if (verbose) cat("nspec:", nspec, "\n")
#
#    maxpointspec_act = readBin(strm, "integer", n=1)
#    if (verbose) cat("maxpointspec_act:", maxpointspec_act, "\n")
#
#    seek(strm, 8, origin="current")        
#   
#    species.name = vector("character", nspec) 
#    if (verbose) cat("Looping species\n")
#    for (i in 1:nspec){
#        tmp = readBin(strm, "integer", n=1)
#        if (verbose) cat(tmp, "\n")
#
#        test = readBin(strm, "character", n=1)        
#        if (verbose) cat(test, "\n")        
#        seek(strm, 6, origin="current")    
#
#        tmp = readBin(strm, "integer", n=1)
#        if (verbose) cat(tmp, "\n")        
#
#        test = readBin(strm, "byte", n=1)        
#        if (verbose) cat(test, "\n")        
#        seek(strm, 6, origin="current")    
#        tmp = readBin(strm, "integer", n=1)
#        if (verbose) cat(tmp, "\n")        
#        test = readBin(strm, "byte", n=1)        
#        if (verbose) cat(test, "\n")            
#        species.name[i] = gsub("[ ]+", "", substr(test, 1, nchar(test)-1))
#
#        seek(strm, 6, origin="current")    
#    }
#    if (verbose) cat("End of species loop\n")
#
#    
#    numpoint = readBin(strm, "integer", n=1, size=4)
#    if (verbose) cat("numpoint:", numpoint, "\n")
#
#    ireleasestart = as.chron(1.1, out.format=c("y-m-d", "h:m:s"))
#    ireleaseend = as.chron(1.1, out.format=c("y-m-d", "h:m:s"))
#    kindz = vector("integer", numpoint)
#    npart = vector("integer", numpoint)
#    rel.com = vector("character", numpoint)
#    rel.lat1 = rep(NA, numpoint)
#    rel.lat2 = rep(NA, numpoint)
#    rel.lng1 = rep(NA, numpoint)
#    rel.lng2 = rep(NA, numpoint)
#    rel.zz1 = rep(NA, numpoint)
#    rel.zz2 = rep(NA, numpoint)
#    
#    seek(strm, 8, origin="current")
#   #    release point information 
#    if (verbose) cat("Loop over release points\n")
#    for (i in 1:numpoint){
#        if (verbose) cat(i, ". release point:\n")
#        # start and end of release relative to simulation end/start
#        tmp = readBin(strm, "integer", n=1, size=4)
#        ireleasestart[i] = as.chron(dtm + tmp/86400, out.format=c("y-m-d", "h:m:s"))
#        if (verbose) cat("release start:", as.character(ireleasestart[i]), "\n")            
#
#        tmp = readBin(strm, "integer", n=1, size=4)
#        ireleaseend[i] = as.chron(dtm + tmp/86400, out.format=c("y-m-d", "h:m:s"))
#        if (verbose) cat("release end:", as.character(ireleaseend[i]), "\n")    
#
#        # kind of vertical coordinate
#        kindz[i] = readBin(strm, "integer", n=1, size=2)
#        if (verbose) cat("kindz:", kindz[i], "\n")    
#        
#        seek(strm, 8, origin="current")
#        #      
#        tmp = readBin(strm, "numeric", size=4, n=6)
#
#        rel.lng1[i] = tmp[1]
#        rel.lat1[i] = tmp[2]
#        rel.lng2[i] = tmp[3]
#        rel.lat2[i] = tmp[4]
#        rel.zz1[i] = tmp[5]
#        rel.zz2[i] = tmp[6]
#        if (verbose) {
#            cat("Lng:", rel.lng1[i], "-", rel.lng2[i], "\n")
#            cat("Lat:", rel.lat1[i], "-", rel.lat2[i], "\n")
#            cat("Alt:", rel.zz1[i], "-", rel.zz2[i])
#            if (kindz[i]==2) cat("m ASL\n") else cat("m AMG\n")
#        }
#        
#        seek(strm, 8, origin="current")
#        npart[i] = readBin(strm, "integer", n=1)
#        if (verbose) cat("npart:", npart[i], "\n")    
#        dummy = readBin(strm, "integer", n=1)
#        seek(strm, 8, origin="current")
#        
#        rel.com[i] = readBin(strm, "character", n=1)
#        if (verbose) cat("rel comment:", rel.com[i], "\n")    
#                
#        seek(strm, 12, origin="current")
#        
#        if (verbose) cat("Loop over species\n")
#        for (j in 1:nspec){            
#            if (verbose) cat(j, ". species:\n")
#            tmp = readBin(strm, "numeric", size=4, n=1)
#            if (verbose) cat("tmp:", tmp, "\n")    
#            seek(strm, 8, origin="current")
#            tmp = readBin(strm, "numeric", size=4, n=1)
#            if (verbose) cat("tmp:", tmp, "\n")    
#            seek(strm, 8, origin="current")
#            tmp = readBin(strm, "numeric", size=4, n=1)
#            if (verbose) cat("tmp:", tmp, "\n")    
#            seek(strm, 8, origin="current")
#        }        
#        if (verbose) cat("End of species loop\n")        
#        
#    }        
#    if (verbose) cat("End of release points loop\n")            
#    
#    tmp = readBin(strm, "integer", n=5)
#
#    seek(strm, 8, origin="current")
#    
#    nageclass = readBin(strm, "integer", n=1)
#    if (verbose) cat("nageclass:", nageclass, "\n")
#    lage = readBin(strm, "integer", n=nageclass)
#    if (verbose) cat("lage:", lage, "\n")
#    
#    oroout = matrix(NA, numygrid, numxgrid)
#    for (i in 1:numxgrid){
#        seek(strm, 8, origin="current")
#        tmp = readBin(strm, "numeric", size=4, n=numygrid)
#        oroout[, i] = tmp[numygrid:1]
#    }
##    print(readBin(strm, "raw", n=100))
#
#    topo = list(xrng=c(outlon0, outlon0+numxgrid*dxout), yrng=c(outlat0, outlat0+numygrid*dyout), zz=oroout)
#
#    close(strm)
#    hdr = list(numxgrid=numxgrid, 
#            numygrid=numygrid, 
#            numzgrid=numzgrid, 
#            dxout=dxout, 
#            dyout=dyout, 
#            outheight=outheight,
#            numpoint=numpoint, 
#            rel.com=rel.com,
#            rel.lat1 = rel.lat1,
#            rel.lat2 = rel.lat2,
#            rel.lng1 = rel.lng1,
#            rel.lng2 = rel.lng2,
#            rel.zz1 = rel.zz1,
#            rel.zz2 = rel.zz2,
#            rel.z.cor = kindz,
#            rel.start = ireleasestart,
#            rel.end = ireleaseend,
#            nageclass=nageclass, 
#            lage = lage,
#            outlon0=outlon0, 
#            outlat0=outlat0,
#            loutstep=loutstep,
#            loutaver=loutaver,
#            maxpointspec_act = maxpointspec_act,
#            dtm = dtm,
#            nspec = nspec,
#            species.name = species.name,
#            topo=topo)
#    class(hdr) = "FLEXPART_header"
##       finished with header
#    return(hdr)
#}
