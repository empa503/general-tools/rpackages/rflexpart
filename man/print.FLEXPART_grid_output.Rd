\name{print.FLEXPART_grid_output}
\alias{print.FLEXPART_grid_output}
\title{ Print information on FLEXPART grid output }
\description{
    Print FLEXPART grid output information on standard output.
}
\usage{
    print.FLEXPART_grid_output(conc)
}
\details{
    Not all FLEXPART model settings are currently stored in the header file.
}
\arguments{
    \item{conc}{ (list) FLEXPART\_grid\_output object }
}
\author{ stephan.henne@empa.ch }
\keyword{ print }% at least one, from doc/KEYWORDS
