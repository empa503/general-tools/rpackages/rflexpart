\name{plot.yz}
\alias{plot.yz}
\title{ plot longitude - altitude cross section }
\description{
    plots contour plot of residence times in the latitude - altitude cross section
}
\usage{
plot.yz(yz, nlev=100, hlevels, color.palette=special.rainbow, inv.cols=FALSE, 
    asp=NULL, zlim, log.scale=FALSE, sub, plot.release=plot.release, ... )
}
\arguments{
  \item{yz}{ total residence times in cross section }
  \item{nlev}{ (numeric) number of color levels in contour plot }
  \item{hlevels}{ (numeric) which vertical levels should be plotted, default is all }
  \item{color.palette}{ (function) function returning color palette }
  \item{inv.cols}{ (logical) should color order be inverted  }
  \item{asp}{ (numeric) if not NULL, aspect ratio of plot }
  \item{zlim}{ (numeric) range of vertical axis }
  \item{log.scale}{ (logical) should log scale be used for color scale }
  \item{sub}{ (character) subtitle of plot }
  \item{plot.release}{ (logical) should release area be marked }
  \item{\dots}{ other arguments passed to fill.contour }
}
\value{
    None
}
\author{ stephan.henne@empa.ch }
\keyword{ hplot }% __ONLY ONE__ keyword per line
