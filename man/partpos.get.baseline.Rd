% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/partpos.get.baseline.R
\name{partpos.get.baseline}
\alias{partpos.get.baseline}
\title{Interpolate 3D model fields to particle positions}
\usage{
partpos.get.baseline(
  part,
  hdr,
  gctm.FUN,
  gctm.args,
  gctm.fls,
  prod.dir = NULL,
  bg.name,
  max.fields = 50,
  get.sd = FALSE
)
}
\arguments{
\item{prod.dir}{If not NULL individual plots of the fields to interpolate and the particle
positions are created.}
}
\description{
Interpolate 3D model fields to particle positions
}
