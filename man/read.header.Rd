\name{read.header}
\alias{read.header}
\alias{read.header.new}
\title{ read FLEXPART output header file  }
\description{
    Reads header file of FLEXPART output
}
\usage{
read.header(fn, verbose = FALSE)
read.header.new(fn, verbose = FALSE)
}
\arguments{
  \item{fn}{ (character) file name of header file }
  \item{verbose}{ (logical) should additional progress information be printed? }
}
\details{
}
\value{
  \item{numxgrid}{(numeric) number of grid points in longitudinal direction} 
  \item{numygrid}{(numeric) number of grid points in latitudinal direction} 
  \item{numzgrid}{(numeric) number of grid points in vertical direction} 
  \item{dxout}{ (numeric) longitudinal grid distance in degree } 
  \item{dyout}{ (numeric) latitudinal grid distance in degree } 
  \item{outheight}{ (numeric) top of vertical grid levels }
  \item{numpoint}{ (numeric) number of release points } 
  \item{rel.com}{ (character)  releases comment }
  \item{rel.lat1}{ (numeric) latitude of lower left corner of release box }
  \item{rel.lat2}{ (numeric) latitude of upper right corner of release box }
  \item{rel.lng1}{ (numeric) longitude of lower left corner of release box }
  \item{rel.lng2}{ (numeric) longitude of upper right corner of release box }
  \item{rel.zz1}{ (numeric) lower bond of release altitude }
  \item{rel.zz2}{ (numeric) upper bond of release altitdue }
  \item{rel.zcor}{ (integer) identifying the vertical coordiante: 1 m above model ground, 2: m above sea level}
  \item{rel.start}{ (chron) start of release }
  \item{rel.end}{ (chron) end of release }
  \item{nageclasses}{ (numeric) number of different age classes }
  \item{outlon0}{ (numeric) longitude of lower left corner of output grid }  
  \item{outlat0}{ (numeric) latitude of lower left corner of output grid } 
  \item{loutstep}{}
  \item{loutaver}{}
  \item{dtm}{ (chron) start of calculation }
  \item{topo}{ (list) topography of sample grid }
}
\author{ stephan.henne@empa.ch  }
\seealso{ \code{\link{read.grid.time}} }
\keyword{ file }% __ONLY ONE__ keyword per line
