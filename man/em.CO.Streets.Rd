\name{em.CO.Streets}
\alias{em.CO.Streets}
\docType{data}
\title{ CO emissions China }
\description{
    CO emissions inventory by Streets et al. for China and the reference year 2001.
}
\usage{data(em.CO.Streets)}
\format{
  The format is:
List of 4
 \$ xrng : num [1:2]  60.2 157.8
 \$ yrng : num [1:2] -12.8  53.8
 \$ zz   : num [1:134, 1:196] 0 0 0 0 0 0 0 0 0 0 ...
 \$ units: chr "E\_CO (ug/s)"
}
\details{
    Emissions are given in ug/s and grid cell. The grid cells are 0.5 deg by 0.5 deg. 
}
\source{
Streets et al., Revisiting China's CO emissions after the Transport and Chemical 
Evolution over the Pacific (TRACE-P) mission: Synthesis of inventories,
atmospheric modeling, and observations,
J. Geophys. Res., 111, D14306, doi:10.1029/2006JD007118, 2006.
}
\keyword{datasets}
