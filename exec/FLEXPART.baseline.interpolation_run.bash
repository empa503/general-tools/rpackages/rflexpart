#!/bin/bash
#
#	Script to start SBATCH job on node that then executes FLEXPART post-processing 
#		script: FLEXPART.baseline.interpolation.R  in $script_dir (part of Rflexpart R package)
#
#	SBATCH settings to be adjusted below.
#	Execute with sbatch FLEXPART.baseline.interpolation_run_daint.bash
#	Additional arguments will be forwared to R script
#	Any other settings to be adjusted in FLEXPART.baseline.interpolation.R
#	

#SBATCH --job-name=Rjob
#SBATCH --output=slurmjob.%j.o
#SBATCH --error=slurmjob.%j.e
#SBATCH --partition=normal
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=36
#SBATCH --cpus-per-task=1
#SBATCH --time=06:00:00
#SBATCH --constraint=mc
#SBATCH --account=em05

#=======START===========

#	location of R script to execute
script_dir="/users/shenne/git_empa/Rpackages/Rflexpart/exec"

args=("$@")

#	make sure the desired programming environment is available
module load daint-mc
module switch PrgEnv-cray PrgEnv-gnu
module switch gcc/11.2.0 gcc/9.3.0
module load cray-netcdf

export EASYBUILD_PREFIX=/store/empa/em05/shenne/easybuild
module use ${EASYBUILD_PREFIX}/modules/all

module load GDAL
module load PROJ

#	load R module in own easybuild environment
module load cray-R/3.6.3.1

#   just to make sure: set unlimited stack size, should be the default on daint
ulimit -s unlimited

#	start script
Rscript --vanilla ${script_dir}/FLEXPART.baseline.interpolation.R

